﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShortPathToCity.Classes
{
    class Graph<T>
    {
        private readonly List<Vertex<T>> _vertices = new List<Vertex<T>>();
        private readonly List<PathBetweenCity<T>> _paths = new List<PathBetweenCity<T>>();

        public IEnumerable<Vertex<T>> Vertices => _vertices.AsEnumerable();
        public IEnumerable<PathBetweenCity<T>> Paths => _paths.AsEnumerable();

        public void AddVertex(Vertex<T> vertex)
        {
            _vertices.Add(vertex);
        }

        public void RemoveVertex(Vertex<T> vertex)
        {
            _vertices.Remove(vertex);
        }

        public void AddPathBetweenCity(PathBetweenCity<T> path)
        {
            _paths.Add(path);
            _vertices.Single(t => t == path.Node1).AddNeighbor(path);
            _vertices.Single(t => t == path.Node2).AddNeighbor(path);
        }

        public void RemovePAthBetweenCity(PathBetweenCity<T> path)
        {
            _vertices.Single(t => t == path.Node1).RemoveNeighbor(path);
            _vertices.Single(t => t == path.Node2).RemoveNeighbor(path);
            _paths.Remove(path);
        }
    }
}
