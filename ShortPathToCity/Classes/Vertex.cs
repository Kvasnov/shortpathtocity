﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShortPathToCity.Classes
{
    class Vertex<T>
    {
        private readonly T _data;
        private readonly List<PathBetweenCity<T>> _neighbors = new List<PathBetweenCity<T>>();
        public Vertex(T data)
        {
            _data = data;
        }

        public T Data => _data;
        public IEnumerable<PathBetweenCity<T>> Neighbors => _neighbors.AsEnumerable();

        public void AddNeighbor(PathBetweenCity<T> path)
        {
            _neighbors.Add(path);
        }

        public void RemoveNeighbor(PathBetweenCity<T> path)
        {
            _neighbors.Remove(path);
        }
    }
}
