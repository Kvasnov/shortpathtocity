﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShortPathToCity.Classes
{
    class PathBetweenCity<T>
    {
        private readonly Vertex<T> _node1;
        private readonly Vertex<T> _node2;

        public double Weight { get; set; }

        public PathBetweenCity(Vertex<T> node1, Vertex<T> node2)
        {
            _node1 = node1;
            _node2 = node2;
        }

        public Vertex<T> Node1 => _node1;
        public Vertex<T> Node2 => _node2;
    }
}
