﻿/*

Задание:
Реализовать структуру данных хранения некоторого набора городов, а также расстояний между парами городов, между которыми есть дорога. 
Найти кратчайший путь между двумя заданными городами

*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShortPathToCity.Classes;

namespace ShortPathToCity
{
    class Program
    {
        static void Main(string[] args)
        {

            var graph = new Graph<City>();
            var vertex1 = new Vertex<City>(new City { Name = "Moscow" });
            var vertex2 = new Vertex<City>(new City { Name = "Penza" });
            var vertex3 = new Vertex<City>(new City { Name = "St. Petersburg" });
            var vertex4 = new Vertex<City>(new City { Name = "Cherepovets" });
            var vertex5 = new Vertex<City>(new City { Name = "Omsk" });
            var vertex6 = new Vertex<City>(new City { Name = "Tver" });
            var vertex7 = new Vertex<City>(new City { Name = "Vologda" });
            var vertex8 = new Vertex<City>(new City { Name = "Novosibirsk" });

            graph.AddVertex(vertex1);
            graph.AddVertex(vertex2);
            graph.AddVertex(vertex3);
            graph.AddVertex(vertex4);
            graph.AddVertex(vertex5);
            graph.AddVertex(vertex6);
            graph.AddVertex(vertex7);
            graph.AddVertex(vertex8);

            var pathMP = new PathBetweenCity<City>(vertex1, vertex2) { Weight = 500d };
            var pathMS = new PathBetweenCity<City>(vertex1, vertex3) { Weight = 400d };
            var pathMC = new PathBetweenCity<City>(vertex1, vertex4) { Weight = 100d };
            var pathPO = new PathBetweenCity<City>(vertex2, vertex5) { Weight = 1000d };
            var pathSO = new PathBetweenCity<City>(vertex3, vertex5) { Weight = 30d };
            var pathCO = new PathBetweenCity<City>(vertex4, vertex5) { Weight = 111d };
            var pathOT = new PathBetweenCity<City>(vertex5, vertex6) { Weight = 555d };
            var pathOV = new PathBetweenCity<City>(vertex5, vertex7) { Weight = 678d };
            var pathTN = new PathBetweenCity<City>(vertex6, vertex8) { Weight = 2100d };
            var pathVN = new PathBetweenCity<City>(vertex7, vertex8) { Weight = 2500d };


            graph.AddPathBetweenCity(pathMP);
            graph.AddPathBetweenCity(pathMS);
            graph.AddPathBetweenCity(pathMC);
            graph.AddPathBetweenCity(pathPO);
            graph.AddPathBetweenCity(pathSO);
            graph.AddPathBetweenCity(pathCO);
            graph.AddPathBetweenCity(pathOT);
            graph.AddPathBetweenCity(pathOV);
            graph.AddPathBetweenCity(pathTN);
            graph.AddPathBetweenCity(pathVN);


            double fullPAth = 0;

            var startCity = vertex1;
            var finishCity = vertex8;
            Vertex<City> next = null;
            Vertex<City> last = null;
            

            var stack = new Stack<Vertex<City>>();
            var stackPath = new Stack<double>();
            stack.Push(startCity);
            stackPath.Push(0); // чтобы стек длины дороги удалялся одновременно с stack городов
            while (stack.Any())
            {
                
                var current = stack.Peek();

                var node = current.Neighbors
                    .OrderBy(t => t.Node2.Data.Name)
                    .FirstOrDefault(t => !stack.Contains(t.Node2) && 
                                         (last == null || t.Node2.Data.Name.CompareTo(last.Data.Name) > 0));

                next = node?.Node2;

                var node1 = node?.Node1;


                if (next != null)
                {
                    stack.Push(next);
                    stackPath.Push(graph.Paths.Where(_ => (_.Node1 == node1 && _.Node2 == next) || (_.Node1 == next && _.Node2 == node1)).First().Weight); 
                    
                    last = null;
                    if (stack.Peek() == finishCity)
                    {
                        //Возможные пути:
                        //Console.WriteLine(string.Join(", ", stack.Reverse().Select(t => t.Data.Name)));
                        if (fullPAth > stackPath.Sum() || fullPAth == 0)
                        {
                            fullPAth = stackPath.Sum();
                        }
                        last = stack.Peek();
                    }
                }
                else
                {
                    last = stack.Pop();
                    stackPath.Pop();
                }
                
            }

            if (fullPAth > 0)
            {
                Console.WriteLine($"The shortest path between {startCity.Data.Name} and {finishCity.Data.Name} = {fullPAth}");
            }
            else
            {
                Console.WriteLine($"The path between {startCity.Data.Name} and {finishCity.Data.Name} not found");
            }
            Console.ReadKey();
        }
    }
    
}
